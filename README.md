# Binary Tree

![ScreenShot](https://bitbucket.org/yinghanwang/binarytree/raw/42f021a29e90064c2cd44bec04ad079f3e000ce4/screenshot.jpg)

Link:
[<strong><a href="https://cdn.rawgit.com/wangx6/BinaryTree/master/index.html">Click here to view 1 binary tree</a></strong>]

[<strong><a href="https://cdn.rawgit.com/wangx6/BinaryTree/forest/index.html">Click here to view binary forest</a></strong>]
